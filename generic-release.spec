%define debug_package %{nil}
%define product_family %{?_vendor:%{_vendor}}%{!?_vendor:openEuler}
%define variant_titlecase Server
%define variant_lowercase server
%define dist_release_version 8
%define generic_version 20.03
%define generic_patch_level LTS
%define generic_release 55
%define builtin_release_version 1.0

%define current_arch %{_arch}
%ifarch i386
%define current_arch x86
%endif

Name:		%{product_family}-release
Version:	%{generic_version}%{generic_patch_level}
Release:	%{generic_release}
Summary:	%{product_family} release file
License:	Mulan PSL v2
Provides:	%{product_family}-release
Provides:	%{_vendor}-release
Provides:	generic-release
Provides:	system-release = %{generic_version}%{generic_patch_level}
Source0:	generic-release-%{builtin_release_version}.tar.gz
Source1:	85-display-manager.preset
Source2:	90-default.preset
Source3:	99-default-disable.preset

BuildRequires:	python3
Requires:       %{product_family}-repos bc
Obsoletes: 	yum-utils yum-utils-help yum-utils-plugins %{product_family}-performance

%description
%{product_family} release files

%prep
%setup -q -n generic-release-%{builtin_release_version}

%build
echo OK

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc
echo "%{product_family} release %{generic_version} (%{generic_patch_level})" > $RPM_BUILD_ROOT/etc/%{product_family}-release
ln -sf %{product_family}-release $RPM_BUILD_ROOT/etc/system-release

# create /etc/os-release
%define minimized_id `echo %{product_family} | tr '[:upper:]' '[:lower:]'`
cat << EOF >>$RPM_BUILD_ROOT/etc/os-release
NAME="%{product_family}"
VERSION="%{generic_version} (%{generic_patch_level})"
ID="%{minimized_id}"
VERSION_ID="%{generic_version}"
PRETTY_NAME="%{product_family} %{generic_version} (%{generic_patch_level})"
ANSI_COLOR="0;31"

EOF

mkdir -p -m 755 $RPM_BUILD_ROOT/etc/profile.d
cp -a system-info.sh $RPM_BUILD_ROOT/etc/profile.d/

# write cpe to /etc/system/release-cpe
echo "cpe:/o:%{product_family}:%{product_family}:%{version}:ga:server" > $RPM_BUILD_ROOT/etc/system-release-cpe

# create /etc/issue and /etc/issue.net
echo '\S' > $RPM_BUILD_ROOT/etc/issue
echo 'Kernel \r on an \m' >> $RPM_BUILD_ROOT/etc/issue
cp $RPM_BUILD_ROOT/etc/issue $RPM_BUILD_ROOT/etc/issue.net
echo >> $RPM_BUILD_ROOT/etc/issue

mkdir -p $RPM_BUILD_ROOT/usr/share/eula
cp eula.[!py]* $RPM_BUILD_ROOT/usr/share/eula

mkdir -p $RPM_BUILD_ROOT/var/lib
cp supportinfo $RPM_BUILD_ROOT/var/lib/supportinfo

# set up the dist tag macros
install -d -m 755 $RPM_BUILD_ROOT/etc/rpm
cat >> $RPM_BUILD_ROOT/etc/rpm/macros.dist << EOF
# dist macros.

%%dist %%{nil}
%%%{product_family} 2 
EOF

# use unbranded datadir
mkdir -p -m 755 $RPM_BUILD_ROOT/%{_datadir}/%{product_family}-release
install -m 644 EULA $RPM_BUILD_ROOT/%{_datadir}/%{product_family}-release

# use unbranded docdir
mkdir -p -m 755 $RPM_BUILD_ROOT/%{_docdir}/%{product_family}-release
cp -p License/LICENSE $RPM_BUILD_ROOT/%{_docdir}/%{product_family}-release

# copy systemd presets
mkdir -p %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE1} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE2} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE3} %{buildroot}%{_prefix}/lib/systemd/system-preset/

%clean
rm -rf $RPM_BUILD_ROOT


%post
rpm -q openEuler-performance > /dev/null
if [ $? -eq 0 ]; then
  cp /etc/profile.d/performance.sh /etc/profile.d/performance.sh.bak
  echo -e "\033[31m Attention: openEuler-performance package files are backed up to /etc/profile.d/performance.sh.bak \033[0m"
fi

%files
%defattr(0644,root,root,0755)
/etc/system-release
/etc/profile.d/system-info.sh
/etc/%{product_family}-release
%config(noreplace) /etc/os-release
%config /etc/system-release-cpe 
%config(noreplace) /etc/issue
%config(noreplace) /etc/issue.net
/etc/rpm/macros.dist
%{_docdir}/%{product_family}-release/*
%{_datadir}/%{product_family}-release/*
%{_prefix}/lib/systemd/system-preset/*
/var/lib/supportinfo
/usr/share/eula/eula.*

%changelog
* Mon Aug 26 2024 Wei Jiangang <wei_jiangang@hoperun.com> - 1.0-55
- Fix wrong ID that does not meet standards

* Tue May 28 2024 liyanan <liyanan61@h-partners.com> - 1.0-54
- Delete Processes print

* Mon Dec 12 2022 wangkai <wangkai385@h-partners.com> - 1.0-53
- Delete package openEuler-performance

* Tue Nov 8 2022 yangmingtai <yangmingtai@huawei.com> - 1.0-52
- delete color.sh

* Tue Nov 8 2022 liyanan <liyanan32@h-partners.com> - 1.0-51
- sysconfig parameters have a impact on performance,comment out the relevant content

* Fri Jul 15 2022 xiasenlin <xiasenlin1@huawei.com> - 1.0.50
- Update EULA license for donate

* Wed Dec 01 2021 yaokai <yaokai13@huawei.com> - 1.0.49
- Fix syntax errors

* Mon Nov 29 2021 yaokai <yaokai13@huawei.com> - 1.0.48
- recover the dependency for bc

* Mon Nov 22 2021 miao_kaibo <miao_kaibo@outlook.com> - 1.0-47
- delete dependency bc

* Thu Aug 26 2021 wangxiaorou <wangxiaorou@uniontech.com> - 1.0-46
- fix system-info.sh syntax error

* Thu Apr  1 2021 gaochong <gaochong@uniontech.com> - 1.0-45
- bugfix I468W9

* Thu Apr  1 2021 gaochong <gaochong@uniontech.com> - 1.0-44
- get memeroy info from /proc/meminfo instead of command free, fix issue I3EN1Z

* Sat Dec 26 2020 fu_changjie <fuchangjie@kylinos.cn> - 1.0-43
- Change value of product_family to 2, fix issue I3A6CP

* Sat Dec 26 2020 fu_changjie <fuchangjie@kylinos.cn> - 1.0-42
- Use generic_version macro instead of constant value， fix issue I28WC0

* Sat Dec 26 2020 Shinwell_Hu <huxinwei@huawei.com> - 1.0-41
- Disable GOMP_CPU_AFFINITY setting in performance.sh to fix issue I1OT20

* Thu Aug 20 2020 co <colo9@outlook.com> - 1.0-40
- fix bad date format in changelog 
- /sbin/ip not found err fix

* Thu Aug 6 2020 zhangrui <zhangrui182@huawei.com> - 1.0-39
- modify system-info.sh to show more ip address when login

* Fri Jun 19 2020 shenyangyang <shenyangyang4@huawei.com> - 1.0-38
- change build requires of python to python3

* Mon May 18 2020 shenyangyang <shenyangyang4@huawei.com> - 1.0-37
- add equal sign in color.sh

* Fri May 15 2020 shenyangyang <shenyangyang4@huawei.com> - 1.0-36
- Add more default color

* Thu May 7 2020 shenyangyang <shenyangyang4@huawei.com> - 1.0-35
- Modify the license info in files

* Wed Apr 29 2020 shenyangyang <shenyangyang4@huawei.com> - 1.0-34
- Change license of mulan to v2

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 1.0-33
- add obsoletes the yum utils

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-32
- change generic_release_1.0

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-31
- change generic_version

* Tue Mar 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-30
- add color aliases

* Tue Mar 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-29
- fix changelog

* Sun Mar 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-28
- add base

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-27
- fix obs build problem

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-26
- fix install problem

* Fri Mar 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-25
- add performance script,close transparent_hugepage,change vm.min_free_kbytes=112640,export GOMP_CPU_AFFINITY
- close autogroup

* Tue Mar 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-24
- remove unnecessary swap file

* Tue Mar 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-23
- fix welcome information

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-22
- fix welcome information

* Fri Dec 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0-21
- Delete unneeded info

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0-20
- Delete unneeded provides

* Thu Dec 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0-19
- Delete unneeded message

* Tue Oct 15 2019 fanghuiyu <fanghuiyu@huawei.com> - 1.0-18
- Change to generic-release

* Fri Sep 20 2019 hexiaowen <hexiaowen@huawei.com> - 1.0-17
- Provides generic-release

* Sat Aug 31 2019 hexiaowen <hexiaowen@huawei.com> - 1.0-16
- add eula.US

* Mon Aug 26 2019 zhangrui <zhangrui182@huawei.com> - 1.0-15
- Add system-info.sh to /etc/profile.d/ to show some information in welcome screen

* Thu Aug 22 2019 hexiaowen <hexiaowen@huawei.com> - 1.0-14
- add License

* Thu Aug 8 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0-13
- Package init
